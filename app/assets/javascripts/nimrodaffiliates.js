window.Nimrodaffiliates = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},
  initialize: function() {
    window.router = new Nimrodaffiliates.Routers.Taxons({});
    Backbone.history.start();
  }
};

$(document).ready(function(){
  Nimrodaffiliates.initialize();
});
