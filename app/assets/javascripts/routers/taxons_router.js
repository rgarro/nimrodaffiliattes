Nimrodaffiliates.Routers.Taxons = Backbone.Router.extend({
	taxons_list:"xxx",
	initialize:function(options){
		
	},
	routes:{
		"taxons":"taxons",
		"new_taxon":"new_taxon",
		"dashboard":"dashboard",
		"signups":"signups",
		"revenues":"revenues",
		"mediakit":"mediakit"
	},
	taxons:function(){
		var taxons = new Nimrodaffiliates.Collections.Taxons();
		taxons.fetch({success:function(c,r,o){
			console.log(taxons);
			var vista = new Nimrodaffiliates.Views.TaxonsIndex();
			vista.render();
		}});
	},
	new_taxon:function(){
		
	},
	dashboard:function(){
		var vista = new Nimrodaffiliates.Views.DashboardIndex();
		vista.render();
	},
	signups:function(){
		
	},
	revenues:function(){
		
	},
	mediakit:function(){
	
	}
});
