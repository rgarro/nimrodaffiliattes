class PublicController < ApplicationController
  def home
    if !current_user.nil?
      redirect_to manager_dashboard_path + "#dashboard"     
    end
  end

  def about
  end

  def contact
  end
end
