class ManagerController < ApplicationController
  
  before_filter :authenticate_user!
  
  def dashboard
  end

  def affiliates
  end

  def taxonomy
  end

  def content
  end
end
