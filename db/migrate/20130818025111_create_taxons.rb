class CreateTaxons < ActiveRecord::Migration
  def change
    create_table :taxons do |t|
      t.references :user
      t.string :name

      t.timestamps
    end
    add_index :taxons, :user_id
  end
end
