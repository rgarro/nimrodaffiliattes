require 'spec_helper'

describe Taxon do
  
  before :all do
    @test_taxon = Taxon.new({:name=>"test name"})
  end
  
  it "name should be required" do
    @test_taxon.name = ""
    #@test_taxon.should_not be_valid
    @test_taxon.valid?.should be_false
  end
  
end
