require 'spec_helper'

describe ManagerController do

  describe "GET 'dashboard'" do
    it "returns http success" do
      get 'dashboard'
      response.should be_success
    end
  end

  describe "GET 'affiliates'" do
    it "returns http success" do
      get 'affiliates'
      response.should be_success
    end
  end

  describe "GET 'taxonomy'" do
    it "returns http success" do
      get 'taxonomy'
      response.should be_success
    end
  end

  describe "GET 'content'" do
    it "returns http success" do
      get 'content'
      response.should be_success
    end
  end

end
