require 'spec_helper'

describe "taxons/new" do
  before(:each) do
    assign(:taxon, stub_model(Taxon,
      :user => nil,
      :name => "MyString"
    ).as_new_record)
  end

  it "renders new taxon form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => taxons_path, :method => "post" do
      assert_select "input#taxon_user", :name => "taxon[user]"
      assert_select "input#taxon_name", :name => "taxon[name]"
    end
  end
end
