require 'spec_helper'

describe "taxons/index" do
  before(:each) do
    assign(:taxons, [
      stub_model(Taxon,
        :user => nil,
        :name => "Name"
      ),
      stub_model(Taxon,
        :user => nil,
        :name => "Name"
      )
    ])
  end

  it "renders a list of taxons" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
